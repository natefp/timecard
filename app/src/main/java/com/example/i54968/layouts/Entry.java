package com.example.i54968.layouts;

public class Entry
{
    public static int newId = -1;

    long id = newId;  // to keep track of the day (0 is Monday)
    int start;
    int end;
    int breaks;
    boolean isStartBlank = true;
    boolean isEndBlank = true;
    boolean isBreaksBlank = true;

    public long getId()
    {
        return id;
    }

    public void setId(long value)
    {
        id = value;
    }

    public int getStart()
    {
        return start;
    }

    public void setStart(int value)
    {
        start = value;
        isStartBlank = false;
    }

    public int getEnd()
    {
        return end;
    }

    public void setEnd(int value)
    {
        end = value;
        isEndBlank = false;
    }

    public int getBreaks()
    {
        return breaks;
    }

    public void setBreaks(int value)
    {
        breaks = value;
        isBreaksBlank = false;
    }
}
