package com.example.i54968.layouts;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.sql.SQLException;

import static com.example.i54968.layouts.Field.BreakTimeEntry;
import static com.example.i54968.layouts.Field.EndTimeEntry;
import static com.example.i54968.layouts.Field.StartTimeEntry;


/**
 * Created by i54968 on 2/2/15.
 */
public class TimeEntry extends EditText implements View.OnFocusChangeListener, TextWatcher, TimePickerDialog.OnTimeSetListener
{
    private TimeEntryListener listener;
    private Activity activity;
    private boolean isArmyTime;
    private Day day;
    private Field field;  // This control is always either a startTimeEntry or an endTimeEntry.  When Day creates each, it initializes this appropriately.

    public TimeEntry(Context context)
    {
        super(context);
        initialize(context);
    }

    public TimeEntry(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initialize(context);
    }

    public TimeEntry(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context)
    {
        activity = (Activity) context;
        isArmyTime = DateFormat.is24HourFormat(activity);
        this.setOnFocusChangeListener(this);
    }

    public void setListener(TimeEntryListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        String prefKey = getContext().getString(R.string.pref_input_method_key);
        String prefDefault = getContext().getString(R.string.pref_input_method_default);
        String prefPicker = getContext().getString(R.string.pref_picker);
        String prefBoth = getContext().getString(R.string.pref_picker_and_text_entry);

        String inputMethodPref = sharedPrefs.getString(prefKey, prefDefault);

        if(inputMethodPref.equals(prefPicker) || inputMethodPref.equals(prefBoth))
        {
            if (((MyActivity) activity).wasRotated == false)
            {
                if (hasFocus)
                {
                    day.setCurrentDay();
                    day.setCurrentField(((TimeEntry) view).field);
                    showTimePickerDialog(view);
                } else if (view instanceof EditText && listener != null)
                {
                    listener.onFocusChange(this, hasFocus);  // this will calculate time total
                }
            }
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute)
    {
        Entry entry = new Entry();
        entry.setId(day.dayNumber);

        if(field == StartTimeEntry)
        {
            day.setStartTime(hour, minute, isArmyTime);
            this.setText(day.getStartTimeString());

            entry.setStart(day.getStartTotalMinutes());  // store data from this field in the SQLite database
            updateDatasource(entry);    // TODO: refactor to use day instead of Entry?
            listener.onFocusChange(this, true);  // this will calculate time total
        }
        else if(field == EndTimeEntry)
        {
            day.setEndTime(hour, minute, isArmyTime);
            this.setText(day.getEndTimeString());

            entry.setEnd(day.getEndTotalMinutes());  // store data from this field in the SQLite database
            updateDatasource(entry);
            listener.onFocusChange(this, true);  // this will calculate time total
        }
        else if(field == BreakTimeEntry)
        {
            day.setBreaksTime(hour, minute, isArmyTime);
            this.setText(day.getBreaksTimeString());

            entry.setBreaks(day.getBreakTotalMinutes());  // store data from this field in the SQLite database
            updateDatasource(entry);
            listener.onFocusChange(this, true);  // this will calculate time total
        }
    }

    private void updateDatasource(Entry entry)
    {
        try
        {
            Datasource datasource = new Datasource(activity);
            datasource.open();
            datasource.updateEntry(entry);
            datasource.close();
        }
        catch(SQLException e)
        {
            Log.d("Time Card: ", e.getMessage());
        }
    }

    public void showTimePickerDialog(View view)
    {
        TimePickerFragment timePickerFragment = new TimePickerFragment();

        Bundle bundle = new Bundle();

        switch(field)
        {

            case StartTimeEntry:
                if (day.isStartTimeEmpty() == false)
                {
                    bundle.putInt("TotalTimeInMinutes", day.getStartTotalMinutes());
                    bundle.putBoolean("IsArmyTime", day.getIsArmyTimeStart());  // put total number of minutes into bundle
                    timePickerFragment.setArguments(bundle);
                }
                break;

            case EndTimeEntry:
                if (day.isEndTimeEmpty() == false)
                {
                    bundle.putInt("TotalTimeInMinutes", day.getEndTotalMinutes());
                    bundle.putBoolean("IsArmyTime", day.getIsArmyTimeEnd());
                    timePickerFragment.setArguments(bundle);
                }
                break;

            case BreakTimeEntry:
                if (day.isBreakTimeEmpty() == false)
                {
                    bundle.putInt("TotalTimeInMinutes", day.getBreakTotalMinutes());
                    bundle.putBoolean("IsArmyTime", day.getIsArmyTimeBreak());  // put total number of minutes into bundle
                    timePickerFragment.setArguments(bundle);
                }
                break;
        }

        timePickerFragment.SetListener(this);
        timePickerFragment.show(activity.getFragmentManager(), "timePicker");
    }

    public boolean isArmyTime()
    {
        return isArmyTime;
    }

    public void setDay(Day day)
    {
        this.day = day;
    }

    public void setField(Field field)
    {
        this.field = field;
    }

    public Field getField()
    {
        return field;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {

    }

    @Override
    public void afterTextChanged(Editable s)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        String prefKey = getContext().getString(R.string.pref_input_method_key);
        String prefDefault = getContext().getString(R.string.pref_input_method_default);
        String prefText = getContext().getString(R.string.pref_text_entry);
        String prefBoth = getContext().getString(R.string.pref_picker_and_text_entry);

        String inputMethodPref = sharedPrefs.getString(prefKey, prefDefault);

        if(inputMethodPref.equals(prefText) || inputMethodPref.equals(prefBoth))
        {
            // Text modification code to constrain format to an appropriate time format
        }
    }
}