package com.example.i54968.layouts;

import android.app.Activity;
import android.widget.TextView;

/**
 * Created by Nate Hopkins on 1/30/15.
 */
public class Day implements TimeEntryListener
{
    private TimeEntry startTimeEntry;
    private TimeEntry endTimeEntry;
    private TimeEntry breakTimeEntry;  // TODO: constrain the picker to show 24 hour, not AM/PM
    private Time startTime;
    private Time endTime;
    private Time breaksTime;
    private TextView elapsedTimeEntry;
    private int elapsedTime = 0;
    private UpdateTotalListener updateTotalListener;
    public int dayNumber;
    private Activity activity;

    public Day(TimeEntry startTimeEntry, TimeEntry endTimeEntry, TimeEntry breaks, TextView elapsedTimeEntry, dayOfWeek day, Activity activity)
    {
        startTime = new Time();
        endTime = new Time();
        breaksTime = new Time();
        this.startTimeEntry = startTimeEntry;
        this.endTimeEntry = endTimeEntry;
        this.breakTimeEntry = breaks;
        this.elapsedTimeEntry = elapsedTimeEntry;
        this.dayNumber = day.ordinal();
        this.activity = activity;

        this.startTimeEntry.setDay(this);  // give a reference to this day to startTimeEntry
        this.endTimeEntry.setDay(this);
        this.breakTimeEntry.setDay(this);
        this.startTimeEntry.setField(Field.StartTimeEntry);
        this.endTimeEntry.setField(Field.EndTimeEntry);
        this.breakTimeEntry.setField(Field.BreakTimeEntry);

        this.startTimeEntry.setListener(this);
        this.endTimeEntry.setListener(this);
        this.breakTimeEntry.setListener(this);
    }

    public void SetListener(UpdateTotalListener listener)
    {
        updateTotalListener = listener;
    }

    private void calculateRow()
    {
        if(!startTime.isEmpty && !endTime.isEmpty)
        {
            int diff = endTime.subtract(startTime);  // in minutes

            if(!breaksTime.isEmpty)
            {
                diff -= breaksTime.getTotalTimeInMinutes();  // in minutes
            }

            if (diff > 0)
            {
                elapsedTimeEntry.setText(Time.formatTotalFromMinutes(diff));
                elapsedTime = diff;
                updateTotalListener.updateTotal();
            }
            else
            {
                elapsedTimeEntry.setText("End < Start!");
            }
        }
    }

    public void setCurrentDay()
    {
        ((MyActivity) activity).setCurrentDay(dayNumber);
    }

    public void setCurrentField(Field field)
    {
        ((MyActivity) activity).setCurrentField(field);
    }

    public void setFocus(Field field)
    {
        if(field == Field.StartTimeEntry)
        {
            ((MyActivity) activity).setFocus(startTimeEntry);
        }
        else if(field == Field.EndTimeEntry)
        {
            ((MyActivity) activity).setFocus(endTimeEntry);
        }
        else if(field == Field.BreakTimeEntry)
        {
            ((MyActivity) activity).setFocus(breakTimeEntry);
        }
    }

    public boolean containsStartData()
    {
        if(!startTime.isEmpty)
        {
            return true;
        }
        return false;
    }

    public boolean containsEndData()
    {
        if(!endTime.isEmpty)
        {
            return true;
        }
        return false;
    }

    public boolean containsBreakData()
    {
        if(!breaksTime.isEmpty)
        {
            return true;
        }
        return false;
    }

    public int getStartTotalMinutes()
    {
        return startTime.getTotalTimeInMinutes();
    }

    public void setStartTotalMinutes(int startTime)
    {
        if(startTime != 0)
        {
            this.startTime.InitializeTime(startTime);
        }
    }

    public void setStartTime(int hoursArmy, int minutes, boolean isArmyTime)
    {
        startTime.initialize(hoursArmy, minutes, isArmyTime);
    }

    public void setEndTime(int hoursArmy, int minutes, boolean isArmyTime)
    {
        endTime.initialize(hoursArmy, minutes, isArmyTime);
    }

    public void setBreaksTime(int hoursArmy, int minutes, boolean isArmyTime)  // TODO: refactor using a func to choose startTime, endTime, or breaksTime
    {
        breaksTime.initialize(hoursArmy, minutes, isArmyTime);
    }

    public String getStartTimeString()
    {
        return startTime.getTimeString();
    }

    public void setStartTime(int value)
    {
        startTime.setTime(value);
    }

    public String getEndTimeString()
    {
        return endTime.getTimeString();
    }

    public String getBreaksTimeString()
    {
        return breaksTime.getTimeString();
    }

    public void setEndTime(int value)
    {
        endTime.setTime(value);
    }

    public void setBreaksTime(int value)
    {
        breaksTime.setTime(value);
    }

    public boolean isStartTimeEmpty()
    {
        return startTime.isEmpty;
    }

    public boolean isEndTimeEmpty()
    {
        return endTime.isEmpty;
    }

    public boolean isBreakTimeEmpty()
    {
        return startTime.isEmpty;
    }

    public int getEndTotalMinutes()
    {
        return endTime.getTotalTimeInMinutes();
    }

    public void setEndTotalMinutes(int endTime)
    {
        if(endTime != 0)
        {
            this.endTime.InitializeTime(endTime);
        }
    }

    public void setBreaksTotalMinutes(int breaksTime)
    {
        if(breaksTime != 0)
        {
            this.breaksTime.InitializeTime(breaksTime);
        }
    }

    public int getBreakTotalMinutes()
    {
        return breaksTime.getTotalTimeInMinutes();
    }

    public void refreshStart()
    {
        startTimeEntry.setText(getStartTimeString());
        calculateRow();
    }

    public void refreshEnd()
    {
        endTimeEntry.setText(getEndTimeString());
        calculateRow();
    }

    public void refreshBreaks()
    {
        breakTimeEntry.setText(getBreaksTimeString());
        calculateRow();
    }

    public int getElapsedTime()
    {
        return elapsedTime;
    }

    public boolean getIsArmyTimeStart()
    {
        return startTime.getIsArmyTime();
    }

    public boolean getIsArmyTimeEnd()
    {
        return endTime.getIsArmyTime();
    }

    public boolean getIsArmyTimeBreak()
    {
        return breaksTime.getIsArmyTime();
    }

    @Override
    public void onFocusChange(TimeEntry sender, boolean hasFocus)
    {
        calculateRow();
//        SetDayFocused();
    }
}