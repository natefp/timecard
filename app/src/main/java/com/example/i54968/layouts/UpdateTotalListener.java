package com.example.i54968.layouts;

/**
 * Created by i54968 on 2/5/15.
 */
public interface UpdateTotalListener
{
    public void updateTotal();
}
