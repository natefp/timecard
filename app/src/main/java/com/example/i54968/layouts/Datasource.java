package com.example.i54968.layouts;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Datasource
{
    SQLiteDatabase database;
    SQLiteHelper helper;
    String[] allColumns = { SQLiteHelper.COLUMN_ID,
                            SQLiteHelper.COLUMN_START,
                            SQLiteHelper.COLUMN_END,
                            SQLiteHelper.COLUMN_BREAKS};

    public Datasource(Context context)
    {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException
    {
        database = helper.getWritableDatabase();
    }

    public void close()
    {
        helper.close();
    }

    public Entry updateEntry(Entry entry)
    {
        ContentValues values = new ContentValues();
        if(!entry.isStartBlank)
        {
            values.put(SQLiteHelper.COLUMN_START, entry.getStart());
        }
        if(!entry.isEndBlank)
        {
            values.put(SQLiteHelper.COLUMN_END, entry.getEnd());
        }
        if(!entry.isBreaksBlank)
        {
            values.put(SQLiteHelper.COLUMN_BREAKS, entry.getBreaks());
        }
        if(entry.getId() == Entry.newId)
        {
            entry.setId(database.insert(SQLiteHelper.TABLE_ENTRIES, null, values));
        }
        else
        {
            database.update(SQLiteHelper.TABLE_ENTRIES, values, "_id = " + entry.getId(), null);
        }
        Cursor cursor = database.query(SQLiteHelper.TABLE_ENTRIES, allColumns,
                SQLiteHelper.COLUMN_ID + " = " + entry.getId(), null, null, null, null);
        cursor.moveToFirst();
        Entry newEntry = cursorToEntry(cursor);
        cursor.close();
        return newEntry;
    }

    public Map<Integer,Entry> getEntries()  // Retrieve entries from database
    {
        Map<Integer,Entry> entries = new HashMap<>();
        Cursor cursor = database.query(SQLiteHelper.TABLE_ENTRIES, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast())
        {
            Entry entry = cursorToEntry(cursor);
            entries.put((int)entry.getId(), entry);
            cursor.moveToNext();
        }
        cursor.close();
        return entries;
    }

    Entry cursorToEntry(Cursor cursor)
    {
        Entry entry = new Entry();
        entry.setId(cursor.getLong(0));
        if(cursor.isNull(1) == false)
        {
            entry.setStart(cursor.getInt(1));
        }
        if(cursor.isNull(2) == false)
        {
            entry.setEnd(cursor.getInt(2));
        }
        if(cursor.isNull(3) == false)
        {
            entry.setBreaks(cursor.getInt(3));
        }
        return entry;
    }
}
